[CmdletBinding()]
Param(
     [Parameter(Mandatory=$True)]
     #[ValidateScript({Test-Path -Path $_ -PathType Leaf})]
     [string]$sourceFile

     # ,[Parameter(Mandatory=$True)]
     # [ValidateScript({-not(Test-Path -Path $_ -PathType Leaf)})]
     # [string]$destinationFile
) 

function join($path)
{
    $files = Get-ChildItem -Path "$path.*.part" | Sort-Object -Property @{Expression={
        $shortName = [System.IO.Path]::GetFileNameWithoutExtension($_.Name)
        $extension = [System.IO.Path]::GetExtension($shortName)
        if ($extension -ne $null -and $extension -ne '')
        {
            $extension = $extension.Substring(1)
        }
        [System.Convert]::ToInt32($extension)
    }}
    $writer = [System.IO.File]::OpenWrite($path)
    foreach ($file in $files)
    {
		Write-Host "Appending " + $file
        $bytes = [System.IO.File]::ReadAllBytes($file)
        $writer.Write($bytes, 0, $bytes.Length)
		[System.IO.File]::Delete($file)
    }
    $writer.Close()
	Write-Host "Humongous file created :)"
}
Write-Host "Joining humongous file. Please wait..."
join $sourceFile

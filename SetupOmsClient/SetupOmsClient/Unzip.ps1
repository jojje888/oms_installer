Write-Host "Unzipping humongous file..."
Add-Type -A 'System.IO.Compression.FileSystem'; 
[IO.Compression.ZipFile]::ExtractToDirectory('OMS_Data\resources.assets.resS.zip', 'OMS_Data');
Write-Host "Deleting zip-file..."
del OMS_Data\resources.assets.resS.zip;
Write-Host "Deleting script..."
del Unzip.ps1;

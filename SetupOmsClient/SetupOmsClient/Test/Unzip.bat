rem Batch file used to unzip files too big to be included in the package
@echo off
set extract_dir=OMS_Data
set zip_file=OMS_Data\resources.assets.resS.zip
echo Unzipping files. Please wait...
%windir%\system32\windowspowershell\v1.0\powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('%zip_file%', '%extract_dir%'); }"
rem %windir%\system32\windowspowershell\v1.0\powershell.exe -nologo -noprofile -command "& { Start-Process PowerShell -nologo -noprofile -Verb RunAs;  }"
echo Extraction done.

echo Removing the file %zip_file%.
if exist %zip_file% del %zip_file%
if exist Unzip.bat del Unzip.bat
pause

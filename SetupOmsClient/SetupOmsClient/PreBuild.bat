rem Batch file used to unzip the Unity build file before building the installation package
@echo off

rem Command line arguments
rem The folder where the build zip will be extracted to.
rem set extract_dir=%1OmsBuild
rem The Unity build zip file.
rem set zip_file=%2

rem echo The input file %zip_file% will be extracted into %extract_dir%

rem echo Removing the directory %extract_dir%.
rem if exist %extract_dir% rmdir %extract_dir% /s /q

rem echo Extracting. Please wait...
rem %windir%\system32\windowspowershell\v1.0\powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('%zip_file%', '%extract_dir%'); }"
rem echo Extraction done.

rem echo Rename the executable and data folder.
rem if exist "%extract_dir%\Default Windows desktop 64-bit.exe" ren "%extract_dir%\Default Windows desktop 64-bit.exe" OMS.exe
rem if exist "%extract_dir%\Default Windows desktop 64-bit_Data" ren "%extract_dir%\Default Windows desktop 64-bit_Data" OMS_Data

set data_folder=%1OmsBuild\OMS_Data
set problem_child=%data_folder%\resources.assets.resS
set compressed_file=%problem_child%.zip
echo Compressing '%problem_child%' into '%compressed_file%'. Please wait...
%windir%\system32\windowspowershell\v1.0\powershell.exe -nologo -noprofile -command "& { Compress-Archive -Path %problem_child% -CompressionLevel Optimal -DestinationPath %compressed_file }"
rem del %problem_child%

echo Extraction done.


echo Starting installer build...